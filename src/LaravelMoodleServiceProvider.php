<?php

namespace Cadix\LaravelMoodle;

use Illuminate\Support\ServiceProvider;

class LaravelMoodleServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/moodle.php', 'moodle');

        $this->app->singleton(Client::class, function () {
            return new Client();
        });

        $this->app->singleton(Auth::class, function () {
            $client = app(Client::class);

            return new Auth($client);
        });

        $this->app->singleton(Course::class, function () {
            $client = app(Client::class);

            return new Course($client);
        });

        $this->app->singleton(Enrol::class, function () {
            $client = app(Client::class);

            return new Enrol($client);
        });

        $this->app->singleton(User::class, function () {
            $client = app(Client::class);

            return new User($client);
        });
    }

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/moodle.php' => config_path('moodle.php'),
            ], 'config');
        }
    }
}
