<?php

namespace Cadix\LaravelMoodle\Facades;

use Cadix\LaravelMoodle\Course as RootCourse;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootCourse
 *
 * @method static array|null core_course_get_courses(array|null $courses = null)
 * @method static array|null all(array|null $courses = null)
 * @method static array|null core_course_get_courses_by_field(string $field, string|int $value)
 * @method static array|null users(int $course_id)
 */
class Course extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootCourse::class;
    }
}
