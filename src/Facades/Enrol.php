<?php

namespace Cadix\LaravelMoodle\Facades;

use Cadix\LaravelMoodle\Enrol as RootEnrol;
use Illuminate\Support\Facades\Facade;

/**
 * @mixin RootEnrol
 *
 * @method static array|null core_enrol_get_enrolled_users(int $course_id)
 * @method static array|null core_enrol_get_users_courses(int $user_id)
 * @method static bool enrol_manual_enrol_users(array $enrolments)
 * @method static bool create(array $enrolment)
 * @method static bool createMany(array $enrolments);
 */
class Enrol extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return RootEnrol::class;
    }
}
