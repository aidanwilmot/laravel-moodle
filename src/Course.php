<?php

namespace Cadix\LaravelMoodle;

use Cadix\LaravelMoodle\Facades\Enrol;
use GuzzleHttp\Exception\GuzzleException;
use JetBrains\PhpStorm\ExpectedValues;

class Course
{
    public function __construct(public Client $client)
    {
    }

    /**
     * Get all courses
     *
     * @param array|null $courses
     * @return array|null
     * @throws GuzzleException|Exception\MoodleException
     */
    public function core_course_get_courses(array|null $courses = null): array|null
    {
        $this->client->url = 'core_course_get_courses';

        if ($courses && count($courses) > 0) {
            foreach ($courses as $course => $id) {
                $this->client->url .= '&options[ids][' . $course . ']=' . $id;
            }
        }

        return $this->client->request();
    }

    public function all(array|null $courses = null): array|null
    {
        return $this->core_course_get_courses($courses);
    }

    /**
     * @param string $field
     * @param string|int $value
     *
     * @return array|null
     * @throws GuzzleException|Exception\MoodleException
     */
    public function core_course_get_courses_by_field(string $field, string|int $value): array|null
    {
        $this->client->url = 'core_course_get_courses_by_field';
        $this->client->url .= '&field=' . $field . '&value=' . $value;

        $results = $this->client->request();

        if (! is_countable($results) || count($results) === 0) {
            return null;
        }

        return $results[ 'courses' ];
    }

    public function find(
        string|int $value,
        #[ExpectedValues([
            'id',
            'ids',
            'shortname',
            'idnumber',
            'category',
        ])]
        string $field = 'id'
    ): array|null {
        return $this->core_course_get_courses_by_field($field, $value)[ 0 ];
    }

    /**
     * Get enrolled users
     *
     * @param int $course_id
     * @return array|null
     */
    public function users(int $course_id): array|null
    {
        return Enrol::core_enrol_get_enrolled_users($course_id);
    }
}
